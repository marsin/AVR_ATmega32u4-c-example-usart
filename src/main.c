
#ifndef F_CPU
  #define F_CPU 16000000ul
#endif
#ifndef B_UART
  #define B_UART 9600ul
#endif


#include <avr/io.h>
#include <util/delay.h>


#define LED0_PORT PORTB
#define LED0_DDR  DDRB
#define LED0_POS  0     // Onboard RX LED (orange)

#define LED1_PORT PORTD
#define LED1_DDR  DDRD
#define LED1_POS  7     // External LED (on Arduino Digital Pin 6)

#define KEY1_PORT PORTD
#define KEY1_DDR  DDRD
#define KEY1_PIN  PIND
#define KEY1_POS  4     // External key (on Arduino Digital Pin 4)

#define OFF 0
#define ON !0

#define RELEASED 0
#define PRESSED !0


void init(void)
{
//	LED0_DDR  |=  (1<<LED0_POS); // output
//	LED0_PORT &= ~(1<<LED0_POS); // LED off

	LED1_DDR  |=  (1<<LED1_POS); // output
	LED1_PORT &= ~(1<<LED1_POS); // LED off

	KEY1_DDR  &= ~(1<<KEY1_POS); // input
	KEY1_PORT |=  (1<<KEY1_POS); // Pull-Up resistor

	// uart poll
	uint16_t ubrr =	(uint16_t) ((F_CPU / (16.0 * B_UART)) - 0.5);

	UCSR1B = (1<<RXEN1)|(1<<TXEN1);  // enable receiver and transmitter
	UCSR1C = (1<<USBS1)|(3<<UCSZ10); // set frame format: 8data, 2stop bit (8N2)

	UBRR1H = (uint8_t) (ubrr>>8);
	UBRR1L = (uint8_t) ubrr;
}


void set_led0(uint8_t state)
{
	switch (state) {
		case ON:
			LED0_PORT |= (1<<LED0_POS);
			break;
		case OFF:
			LED0_PORT &= ~(1<<LED0_POS);
			break;
		default:
			// do nothing
			break;
	}
}


void set_led1(uint8_t state)
{
	switch (state) {
		case ON:
			LED1_PORT |= (1<<LED1_POS);
			break;
		case OFF:
			LED1_PORT &= ~(1<<LED1_POS);
			break;
		default:
			// do nothing
			break;
	}
}


uint8_t get_key1(void)
{
	if (KEY1_PIN & (1<<KEY1_POS)) {
		return RELEASED;
	} else {
		return PRESSED;
	}
}


/** UART Poll - Transmit Byte.
 *
 * This function polls the UDREx bit of the UCSRxA register.
 * Is it '1', the byte becomes written to the UDRx register.
 *
 * @param  b byte to transmit
 * @return   transmitted character, type casted to int
 */
int uart_poll_transmit_byte(uint8_t b)
{
	while (! (UCSR1A & (1<<UDRE1)));
	UDR1 = b;
	return (int) b;
}


/** UART Poll - Receive Byte (no wait).
 *
 * This function polls **not** the RXCx bit of the UCSRxA register.
 * If RCXx is '1', the byte becomes read from the UDRx register.
 * If RCXx is not '1', the functions returns '-1'.
 *
 * @retval [0..255] received byte
 * @retval -1       no byte received
 */
int uart_poll_receive_byte_nowait(void)
{
	return (UCSR1A & (1<<RXC1)) ? (int) UDR1 : -1;
}


#if TEST == 1
/** A test for the LED and the key. */
int main(void)
{
	init();

	set_led0(ON);
	_delay_ms(500);
	set_led0(OFF);
	_delay_ms(500);

	set_led1(ON);
	_delay_ms(500);
	set_led1(OFF);
	_delay_ms(500);

	while (!0) {
		switch (get_key1()) {
			case PRESSED:
				set_led1(ON);
				break;
			default:
				set_led1(OFF);
				break;
		}
	}

	return 0;
}


#elif TEST == 2
/** Prints ASCII code 'a' to the UART and blinks with the LED. */
int main(void)
{
	init();

	while (!0) {
		set_led1(ON);
		uart_poll_transmit_byte((uint8_t) 'a');
		_delay_ms(250);
		set_led1(OFF);
		_delay_ms(750);
	}

	return 0;
}


#endif // TEST


ATmega32u4 example - USART
==========================

About
-----

  -- USART example implementation for ATmega32u4 on Arduino Micro

Copyright (c) 2017 Martin Singer <martin.singer@web.de>


Circuit
-------

![Image: Circuit TEST 2](./img/circuit.jpg "Circuit")


- The key is connected on Arduino Digital Pin 4
- The LED is connected on Arduino Digital Pin 6


- The TWI bus (SCL, SDA) is not used


### Hardware

#### Controller

- [Atmel AVR ATmega32u4](https://www.microchip.com/wwwproducts/en/ATMEGA32U4)
  ([Data Sheet (PDF)](http://www.atmel.com/Images/Atmel-7766-8-bit-AVR-ATmega16U4-32U4_Datasheet.pdf))


#### Board

- [Arduino Micro](https://www.arduino.cc/en/Main/ArduinoBoardMicro)


##### Pin Mapping

![Image: Arduino Micro pin mapping](./img/arduino_micro_pin_mapping.png "Arduino Micro pin mapping")

- <https://www.arduino.cc/en/uploads/Main/ArduinoMicro_Pinout3.png>
- <https://www.arduino.cc/en/Hacking/PinMapping32u4>


#### USART To USB Bridge

- [myAVR USART to USB bridge](http://shop.myavr.com/?sp=article.sp.php&artID=200024)


#### Other Components

- R1: Resistor 1 - 91  Ohm (pull-up resistor for the switch)
- R2: Resistor 2 - 10K Ohm (fitting to the LED)
- K1: Key 1      - Generic Switch
- D1: LED 1      - Generic Blue LED


Development System Configuration
--------------------------------

### Setup udev rules

Setup a udev rule with unique identifiers for the programmer device!
This can be useful if you have more, similar USB devices connected.


#### Arduino USB Bootloader *(for programming)*

1. Double press the reset button of the Arduino Micro
2. `$ udevadm info -a -p $(udevadm info -q path -n /dev/ttyACM0)`
   *(ensure your Arduino connects on `/dev/ttyACM0`)*
3. Checkout your device attributes
4. `# echo 'KERNEL=="ttyACM[0-9]", SUBSYSTEMS=="usb", ATTRS{idVendor}=="2a03", ATTRS{idProduct}=="0037", ATTRS{bcdDevice}=="0001", SYMLINK+="ttyUSB.arduino_micro"' >> /etc/udev/rules.d/50-mars.rules`
5. `$ udevadm control --reload` or `$ udevadm trigger`


#### USART to USB Bridge

1. Checkout your device attributes
    - *Variant A*
        1. `# lsusb`
        2. Checkout your `<device_id>` and `<bus_id>`
        3. `# lsusb -v -s <bus_id>:<device_id>`
    - *Variant B*
        4. `$ udevadm info -a -p $(udevadm info -q path -n /dev/ttyUSB0)`
           *(ensure your Arduino connects on `/dev/ttyUSB0`)*
3. `# echo 'KERNEL=="ttyUSB[0-9]", SUBSYSTEMS=="usb", ATTRS{idVendor}=="10c4", ATTRS{idProduct}=="ea60", ATTRS{product}=="myAVR - UsbToUart", SYMLINK+="/dev/ttyUSB.UART-bridge.MK3"' >> /etc/udev/rules.d/50-mars.rules`
4. `$ udevadm control --reload` or `$ udevadm trigger`


Documentation
-------------

	$ doxygen doxygen.conf
	$ firefox doc/html/index.html


Compiling and Programming
-------------------------

### Requirements

- avr-gcc
- avrdude


### Usage

- `make all`        Make software.
- `make clean`      Clean out built project files.
- `make program`    Download the hex file to the device, using avrdude.


*If you are using the Arduino USB Bootloader for programming,
double click the reset button of the board,
to setup the MC to programming mode (onboard LED is blinking)!*


### TEST functions

1. *not implemented yet*
2. *not implemented yet*


	$ make clean && make TEST=1
	$ make program


Connect the UART with the serial terminal
-----------------------------------------

	$ picocom --baud 9600 --stopbits 2 /dev/ttyUSB.UART-bridge.MK3

